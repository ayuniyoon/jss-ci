/**
* Template Name: Techie - v3.0.0
* Template URL: https://bootstrapmade.com/techie-free-skin-bootstrap-3/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
!(function($) {
    "use strict";

// Preloader
$(window).on('load', function() {
    if ($('#preloader').length) {
        $('#preloader').delay(100).fadeOut('slow', function() {
            $(this).remove();
        });
    }
});

// Smooth scroll for the navigation menu and links with .scrollto classes
var scrolltoOffset = $('#header').outerHeight() - 16;
if (window.matchMedia("(max-width: 991px)").matches) {
    scrolltoOffset += 16;
}

$(function () {
    $('[data-bs-toggle="tooltip"]').tooltip()
})
$(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function(e) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        e.preventDefault();
        var target = $(this.hash);
        if (target.length) {

            var scrollto = target.offset().top - scrolltoOffset;

            if ($(this).attr("href") == '#header') {
                scrollto = 0;
            }

            $('html, body').animate({
                scrollTop: scrollto
            }, 1500, 'easeInOutExpo');

            if ($(this).parents('.nav-menu, .mobile-nav').length) {
                $('.nav-menu .active, .mobile-nav .active').removeClass('active');
                $(this).closest('li').addClass('active');
            }

            if ($('body').hasClass('mobile-nav-active')) {
                $('body').removeClass('mobile-nav-active');
                $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                $('.mobile-nav-overly').fadeOut();
            }
            return false;
        }
    }
});

// Activate smooth scroll on page load with hash links in the url
$(document).ready(function() {
    if (window.location.hash) {
        var initial_nav = window.location.hash;
        if ($(initial_nav).length) {
            var scrollto = $(initial_nav).offset().top - scrolltoOffset;
            $('html, body').animate({
                scrollTop: scrollto
            }, 1500, 'easeInOutExpo');
        }
    }
});

// Mobile Navigation
if ($('.nav-menu').length) {
    var $mobile_nav = $('.nav-menu').clone().prop({
        class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function(e) {
        $('body').toggleClass('mobile-nav-active');
        $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
        $('.mobile-nav-overly').toggle();
    });

    $(document).on('click', '.mobile-nav .drop-down > a', function(e) {
        e.preventDefault();
        $(this).next().slideToggle(300);
        $(this).parent().toggleClass('active');
    });

    $(document).click(function(e) {
        var container = $(".mobile-nav, .mobile-nav-toggle");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            if ($('body').hasClass('mobile-nav-active')) {
                $('body').removeClass('mobile-nav-active');
                $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                $('.mobile-nav-overly').fadeOut();
            }
        }
    });
} else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
}

// Navigation active state on scroll
var nav_sections = $('section');
var main_nav = $('.nav-menu, #mobile-nav');

$(window).on('scroll', function() {
    var cur_pos = $(this).scrollTop() + 200;

    nav_sections.each(function() {
        var top = $(this).offset().top,
        bottom = top + $(this).outerHeight();

        if (cur_pos >= top && cur_pos <= bottom) {
            if (cur_pos <= bottom) {
                main_nav.find('li').removeClass('active');
            }
            main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
        }
        if (cur_pos < 300) {
            $(".nav-menu ul:first li:first").addClass('active');
        }
    });
});

// Toggle .header-scrolled class to #header when page is scrolled
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('#header').addClass('header-scrolled');
    } else {
        $('#header').removeClass('header-scrolled');
    }
});

if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
}

// Porfolio isotope and filter
$(window).on('load', function() {
    var portfolioIsotope = $('.portfolio-container').isotope({
        itemSelector: '.portfolio-item'
    });

    $('#portfolio-flters li').on('click', function() {
        $("#portfolio-flters li").removeClass('filter-active');
        $(this).addClass('filter-active');

        portfolioIsotope.isotope({
            filter: $(this).data('filter')
        });
        aos_init();
    });

    // Initiate venobox (lightbox feature used in portofilo)
    $(document).ready(function() {
        $('.venobox').venobox();
    });
});

// Init AOS
function aos_init() {
    AOS.init({
        duration: 1000,
        once: true
    });
}
$(window).on('load', function() {
    aos_init();
});

})(jQuery);

// Back to top button
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.back-to-top').fadeIn('slow');
    } else {
        $('.back-to-top').fadeOut('slow');
    }
});

$('.back-to-top').click(function() {
    $('html, body').animate({
        scrollTop: 0
    }, 1500, 'easeInOutExpo');
    return false;
});

// jQuery counterUp
$('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
});

// App Shortcut carousel (uses the Owl Carousel library)
$(".app-popular-carousel").owlCarousel({
    autoplay: false,
    dots: false,
    nav:true,
    loop: false,
    lazyLoad:true,
    margin:0,
    navText : ["<i class='fas fa-chevron-circle-left'></i>", "<i class='fas fa-chevron-circle-right'></i> "],
    responsive: {
        0: {
            items: 2
        },
        768: {
            items: 5
        },
        900: {
            items: 7
        }
    }
});

// APP Category content carousel (uses the Owl Carousel library)
$(".panel-category").owlCarousel({
    autoplay: false,
    dots: false,
    nav: true,
    loop: false,
    lazyLoad:true,
    navText : ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right' ></i>"],
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 3
        },
        900: {
            items: 4
        }
    }
});

// Berita content carousel (uses the Owl Carousel library)
$(".berita-carousel").owlCarousel({
    autoplay: false,
    dots: false,
    nav: true,
    loop: true,
    lazyLoad:true,
    margin: 30,
    navText : ["<i class='fas fa-chevron-circle-left'></i>", "<i class='fas fa-chevron-circle-right' ></i>"],
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 3
        },
        900: {
            items: 4
        }
    }
});

// App tabs content carousel (uses the Owl Carousel library)
$(".app-tabs").owlCarousel({
    autoplay: false,
    dots: false,
    nav: true,
    loop: false,
    autoWidth:true,
    singleItem: true,
    navText : ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right' ></i>"],
    items:5
});



if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
     console.log('iki mobile')
    $('#wisata-artikel').owlCarousel({
        loop: true,
        autoplay: true,
        items: 1,
        nav: false,
        margin: 15,
        autoplayTimeout:4000,
        scrollPerPage: true,
    })
}else{
    console.log('iki web')
    $('#wisata-artikel').owlCarousel({
        loop: true,
        autoplay: true,
        items: 1,
        nav: false,
        smartSpeed : 50,
        autoplayTimeout:4000,
        scrollPerPage: true,
        animateOut: 'slideOutUp',
        animateIn: 'slideInUp'
    })
}

$('.app-tabs li a').click(function() {
    var activeLink = $(this).data('target');
    var targetTab = $('.'+activeLink);

$('.app-tabs li a').removeClass('active'); //hapus di tab-categori
targetTab.siblings().removeClass('active'); // hapus di tab-content
targetTab.addClass('active');
});

// Testimonials carousel (uses the Owl Carousel library)
$(".testimonials-carousel").owlCarousel({
    autoplay: false,
    dots: true,
    loop: true,
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 2
        },
        900: {
            items: 3
        }
    }
});

// Hero carousel
$(".hero-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: true,
    items: 1,
});

// Portfolio details carousel
$(".portfolio-details-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    items: 1
});

// Produk UMKM carousel (uses the Owl Carousel library)
$(".umkm-produk-carousel").owlCarousel({
    autoplay: false,
    dots: false,
    nav: true,
    loop: true,
    lazyLoad:true,
    margin:10,
    navText : ["<i class='fas fa-chevron-circle-left'></i>", "<i class='fas fa-chevron-circle-right'></i>"],
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 3
        },
        900: {
            items: 4
        }
    }
});
var options = {
     theme:"custom",
     content:'<img style="width:80px; color: white" src="'+base_url+'asset/v3/img/logo.png" class="center-block">',
     message:'tunggu, sedang memuat...',
     backgroundColor:"#444444",
     textColor:"white"
};

// start here's public code
function delay(callback, ms)
{
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}
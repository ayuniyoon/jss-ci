<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Beranda extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->module('layout');
	}

	public function index()
	{
		$this->layout->header();
		$this->layout->navbar();
		$this->layout->heroarea();
		$this->load->view('app_pintasan');
		$this->load->view('app_banyak_diakses');
		$this->load->view('app_smart_service');
		$this->load->view('section_berita');
		$this->load->view('section_wisata_budaya');
		$this->load->view('section_promosi');
		$this->load->view('section_pengaduan');
		$this->load->view('section_produk_umkm');
		$this->load->view('section_jss_tv');
		$this->layout->footer();
	}

	public function template()
	{
		$this->load->view('template/jss-small-hero');
	}

	public function login()
	{
		$this->load->view('template/login');
	}

}
<section id="jsstv" class="jsstv section-bg">
    <div class="container">
        <div class="section-title">
            <h2>JSS TV</h2>
            <p>Update informasi Kota Jogja dari siaran</p>
        </div>

        <div class="row">

            <div class="col-xl-8 col-lg-8 d-flex justify-content-center align-items-stretch position-relative mb-4" data-aos="fade-right">
                <div class="big-video-box">
                    <img src="https://img.youtube.com/vi/mT9S3_-i_Ds/maxresdefault.jpg" alt="" class="img-fluid img-content-fit h-100">
                    <a href="https://www.youtube.com/watch?v=mT9S3_-i_Ds" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 icon-boxes d-flex flex-column justify-content-center px-lg-3">
                <div class="row" data-aos="fade-left" data-aos-delay="200">
                    <div class="col-lg-12 col-md-12 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                        <div class="tv-entry">
                            <div class="small-thumb">
                                <img src="https://img.youtube.com/vi/luGZs0dbylw/maxresdefault.jpg" alt="" class="img-fluid img-content-fit h-100">
                                <a href="https://www.youtube.com/watch?v=luGZs0dbylw" class="venobox small-play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                            </div>
                            <h3 class="tv-title">
                                <a href="#">Wayang Jogja Night Carnival (WJNC) #5 Kota Jogja</a>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="row" data-aos="fade-left" data-aos-delay="200">
                    <div class="col-lg-12  col-md-12 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                        <div class="tv-entry">
                            <div class="small-thumb">
                                <img src="https://img.youtube.com/vi/Xv2nASuVvzw/maxresdefault.jpg" alt="" class="img-fluid img-content-fit h-100">
                                <a href="https://www.youtube.com/watch?v=Xv2nASuVvzw" class="venobox small-play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                            </div>
                            <h3 class="tv-title">
                                <a href="#">Focus Group Discussion (Sinergi Pengembangan Inovasi)</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-end mt-4">
            <div class="view-more">
                <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
            </div>
        </div>

    </div>
</section><!-- End jogjaTV -->
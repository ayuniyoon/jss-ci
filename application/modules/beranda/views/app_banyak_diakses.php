<!-- ======= Application Section ======= -->
<section id="app-popular" class="app-popular">
    <div class="container" data-aos="fade-up">

        <div class="border-bottom pb-3">
            <div class="section-title">
                <p>Layanan <span>yang banyak</span> diakses</p>
            </div>

            <div class="mx-2">
                <div class="owl-carousel app-popular-carousel">
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="100" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Kualitas Lingkungan Hidup</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="200" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Pendaftaran Nomor Kebudayaan</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="300" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Pemesanan Mobil Jenazah</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="400" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Info Tagihan PDAM</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="500" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Taman Pintar</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="600" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Surat Keterangan PN</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="700" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Pemesanan Mobil Jenazah</h6>
                    </a>
                    <a href="#" class="app-item" data-aos="fade-up" data-aos-delay="800" >
                        <img class="owl-lazy mx-auto" data-src="<?= base_url()?>assets/img/app.png" alt="">
                        <h6 class="title mt-1">Pendaftaran Seleksi JPT</h6>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End Application Section -->
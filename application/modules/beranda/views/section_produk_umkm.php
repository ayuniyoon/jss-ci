    <!-- ======= UMKM Produk ======= -->
    <section id="umkm-produk" class="umkm-produk">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Produk Jogja</h2>
                <p>Lihat produk dari usaha kecil menengah.</p>
            </div>

            <div class="mx-2">
                <div class="owl-carousel umkm-produk-carousel">
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="100">
                            <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-1.jpg" class="owl-lazy img-fluid img-content-fit h-100" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Udang Level Special Khas Bu Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="200">
                            <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-2.jpg" class="owl-lazy img-fluid img-content-fit h-100" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Special Khas Bu Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="300">
                            <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-3.jpg" class="owl-lazy img-fluid img-content-fit h-100" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="400">
                            <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-4.jpg" class="owl-lazy img-fluid img-content-fit h-100" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Peyeks Level Special Khas Bu Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section><!-- End Pengaduan Section -->
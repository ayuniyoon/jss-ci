<!-- ======= Smart Services Section ======= -->
<section id="smart-service" class="smart-service pb-5">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Smart Service</h2>
            <p>Jelajahi aplikasi dan pelayanan di Kota Yogyakarta</p>
        </div>
        <div class="mb-4">
            <form action="#" method="post" role="form" class="">
                <div class="form-group">
                    <input type="text" class="form-control text-center form-rounded" name="keyword" id="keyword" placeholder="Telusuri aplikasimu disini..." />
                    <div class="validate"></div>
                </div>
            </form>
        </div>

        <div class="" data-aos="fade-up">
            <div class="panel-category owl-carousel" id="panel-category">

                <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="100">
                    <div class="box-category">
                        <div class="img-box"><img data-src="<?= base_url()?>assets/img/category/Kategori eGovernment.png" class="owl-lazy" alt=""></div>
                        <h4 class="title">eGovernment</h4>
                        <hr>
                        <p class="description">Merupakan layanan informasi dan pelayanan pemerintah kepada masyarakat</p>
                    </div>
                </div>
                <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="200">
                    <div class="box-category">
                        <div class="img-box"><img data-src="<?= base_url()?>assets/img/category/Kategori Informasi Pengaduan.png" class="owl-lazy" alt=""></div>
                        <h4 class="title">Pengaduan</h4>
                        <hr>
                        <p class="description">Merupakan layanan ticketing yang ditujukan untuk menerima pelaporan dari masyarakat</p>
                    </div>
                </div>
                <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="300">
                    <div class="box-category">
                        <div class="img-box"><img data-src="<?= base_url()?>assets/img/category/Kategori Kedaruratan.png" class="owl-lazy" alt=""></div>
                        <h4 class="title">Kedaruratan</h4>
                        <hr>
                        <p class="description">Merupakan layanan yang diberikan untuk mendapatkan akses cepat dalam kondisi darurat</p>
                    </div>
                </div>
                <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="400">
                    <div class="box-category">
                        <div class="img-box"><img data-src="<?= base_url()?>assets/img/category/Kategori Layanan Umum.png" class="owl-lazy" alt=""></div>
                        <h4 class="title">Layanan Umum</h4>
                        <hr>
                        <p class="description">Merupakan layanan informasi dan pelayanan pemerintah kepada masyarakat</p>
                    </div>
                </div>
                <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="500">
                    <div class="box-category">
                        <div class="img-box"><img data-src="<?= base_url()?>assets/img/category/Kategori Layanan Umum.png" class="owl-lazy" alt=""></div>
                        <h4 class="title">Layanan Umum</h4>
                        <hr>
                        <p class="description">Merupakan layanan informasi dan pelayanan pemerintah kepada masyarakat</p>
                    </div>
                </div>
            </div>
            <div class="row panel-search-app" id="panel-search-app" style="display: none;">
                <div class="col-lg-3 col-md-6" data-aos="fade-up">
                    <div class="logo-box">
                        <div class="img-app"><img src="<?= base_url()?>assets/img/app.png" class="smart-service-logo" alt=""></div>
                        <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <div class="logo-box">
                        <div class="img-app"><img src="<?= base_url()?>assets/img/app.png" class="smart-service-logo" alt=""></div>
                        <h4 class="title"><a href="">Dolor Sitema</a></h4>
                        <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                    <div class="logo-box">
                        <div class="img-app"><img src="<?= base_url()?>assets/img/app.png" class="smart-service-logo" alt=""></div>
                        <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                        <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
                    <div class="logo-box">
                        <div class="img-app"><img src="<?= base_url()?>assets/img/app.png" class="smart-service-logo" alt=""></div>
                        <h4 class="title"><a href="">Magni Dolores</a></h4>
                        <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Smart Services Section -->

<!-- Modal App -->
<div class="modal fade modal-app" id="modal-app" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-appLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header border-0">
                <img src="<?= base_url()?>assets/img/category/Kategori Kedaruratan.png" class="img-fluid img-header" alt="">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pt-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mx-auto text-center">
                            <h4 class="modal-title" id="modal-appLabel">Kedaruratan</h4>
                            <p class="description">Berbagai macam layanan kegawatdaruratan</p>
                            <hr class="divider">
                        </div>
                    </div>
                    <div class="row mb-3" id="sub-category-1">
                        <div class="col-md-12">
                            <h4 class="service-title">Pilih Sektor</h4>
                        </div>
                        <div class="col-md-3 mt-2 text-center" data-aos="fade-up" data-aos-delay="100">
                            <a href="#" target="_blank">
                                <div class="img-level">
                                    <img src="<?= base_url()?>assets/img/category/mortarboard.png" class="" alt="">
                                    <h4 class="title-level">Pendidikan</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 mt-2 text-center" data-aos="fade-up" data-aos-delay="200">
                            <a href="#" target="_blank">
                                <div class="img-level">
                                    <img src="<?= base_url()?>assets/img/category/baggage.png" class="" alt="">
                                    <h4 class="title-level">Pariwisata</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 mt-2 text-center" data-aos="fade-up" data-aos-delay="200">
                            <a href="#" target="_blank">
                                <div class="img-level">
                                    <img src="<?= base_url()?>assets/img/category/cardiogram.png" class="" alt="">
                                    <h4 class="title-level">Kesehatan</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 mt-2 text-center" data-aos="fade-up" data-aos-delay="200">
                            <a href="#" target="_blank">
                                <div class="img-level">
                                    <img src="<?= base_url()?>assets/img/category/analysis.png" class="" alt="">
                                    <h4 class="title-level">Keuangan</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row mb-3" id="sub-category-2">
                        <div class="col-md-12">
                            <h4 class="service-title">Pilih Kesehatan</h4>
                        </div>
                        <div class="col-md-3 mt-2 text-center" data-aos="fade-up" data-aos-delay="100">
                            <a href="#" target="_blank">
                                <div class="img-level">
                                    <img src="<?= base_url()?>assets/img/category/mortarboard.png" class="" alt="">
                                    <h4 class="title-level">Antrian</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 mt-2 text-center" data-aos="fade-up" data-aos-delay="200">
                            <a href="#" target="_blank">
                                <div class="img-level">
                                    <img src="<?= base_url()?>assets/img/category/baggage.png" class="" alt="">
                                    <h4 class="title-level">Pemesanan</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row mb-3" id="services">
                        <div class="col-md-12">
                            <h4 class="service-title">Layanan</h4>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="100">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Gawat Darurat</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Gawat Darurat dan Kegawatdaruratan Medis (HOTLINE: 119 atau 0274 420118)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="200">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kebakaran</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pemadam Kebakaran (HOTLINE: 0274 587101)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                    </div>                        
                </div>
            </div>
            <div class="modal-footer border-0">
            </div>
        </div>
    </div>
</div>
<!-- ======= Promotion Section ======= -->
    <section id="promotion" class="promotion">
        <div class="container">

            <div class="section-title">
                <h2>Promosi</h2>
                <p>Lihat berbagai macam penawaran menarik disini.</p>
            </div>

            <div class="row">

                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="event d-flex align-items-start" data-aos="fade-up" data-aos-delay="100">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="pic"><img src="<?= base_url()?>assets/img/portfolio/portfolio-1.jpg" class="img-fluid img-content-fit h-100" alt=""></div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="event-info py-2 py-md-0">
                                    <h4>TerPIZZA HEBOH!</h4>
                                    <ul>
                                        <li><i class="fas fa-map-marker-alt"></i> Sekar Kedhaton Resto</li>
                                        <li><i class="fas fa-stopwatch"></i> 2 Hari 4 Jam</li>
                                    </ul>
                                    <p>Dapatkan promo tahun baru kamu jadi makin seru dengan pilihan paket pizza paling hemat</p>
                                </div>
                                <div class="get-it">
                                    <a href="#">Dapatkan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="event d-flex align-items-start" data-aos="fade-up" data-aos-delay="200">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="pic"><img src="<?= base_url()?>assets/img/portfolio/portfolio-2.jpg" class="img-fluid img-content-fit h-100" alt=""></div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="event-info py-2 py-md-0">
                                    <h4>Diskon GEREGETAN sampai dengan 55% - 100%</h4>
                                    <ul>
                                        <li><i class='fas fa-map-marker-alt'></i> Sekar Kedhaton Resto</li>
                                        <li><i class="fas fa-stopwatch"></i> 2 Hari 4 Jam</li>
                                    </ul>
                                    <p>Pesan akomodasi dengan privasi extra untuk pengalaman liburan tak terlupakan</p>
                                </div>
                                <div class="get-it">
                                    <a href="#">Dapatkan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-end mt-5">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section><!-- End Promotion Section -->
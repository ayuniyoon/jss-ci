<!-- ======= Beita Utama ======= -->
<section id="berita" class="berita">
    <div class="container">
        <div class="section-title">
            <h2>Berita Utama</h2>
            <p>Update kabar seputar Kota Yogyakarta</p>
        </div>
        <div class="row highlight mb-4">
            <div class="col-lg-8 col-sm-12 order-1 order-lg-1" data-aos="zoom-in" data-aos-delay="150">
                <div class="img-box">
                    <a href="#" target="_blank"><img src="<?= base_url()?>assets/img/portfolio/portfolio-8.jpg" class="img-fluid img-content-fit h-100" alt=""></a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 pt-4 pt-lg-0 order-2 order-lg-2 content" data-aos="fade-right">
                <div class="date"><span>02/03/2021</span></div>
                <a href="#" target="_blank">
                    <h3>Perlindungan Anak Terpadu Berbasis Masyarakat Tingkatkan Kualitas Keluarga</h3>
                </a>
                <p class="font-italic">
                    Keluarga dan lingkungan masyarakat memiliki peranan penting bagi tumbuh kembang anak. Sebagai generasi penerus bangsa, sudah sepatutnya anak mendapat perlindungan negara dari tindak segala kekerasan.
                </p>
            </div>
        </div>

        <div class="berita-carousel owl-carousel pt-2" id="berita-carousel">
            <div class="item" data-aos="fade-up" data-aos-delay="100">
                <a href="#"  target="_blank" class="my-2">
                    <div class="berita-img">
                        <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-2.jpg" class="owl-lazy img-content-fit h-100" alt="">
                    </div>
                    <div class="date"><span>02/03/2021</span></div>
                    <h3>Perlindungan Anak Terpadu Berbasis Masyarakat Tingkatkan Kualitas Keluarga</h3>
                </a>
                <p class="description">
                    Keluarga dan lingkungan masyarakat memiliki peranan penting bagi tumbuh kembang anak. Sebagai generasi penerus bangsa, sudah sepatutnya anak mendapat perlindungan negara dari tindak segala kekerasan.
                </p>
            </div>
            <div class="item" data-aos="fade-up" data-aos-delay="200">
                <a href="#"  target="_blank" class="my-2">
                    <div class="berita-img">
                        <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-3.jpg" class="owl-lazy img-content-fit h-100" alt="">
                    </div>
                    <div class="date"><span>02/03/2021</span></div>
                    <h3>Perlindungan Anak Terpadu Berbasis Masyarakat Tingkatkan Kualitas Keluarga</h3>
                </a>
                <p class="description">
                    Keluarga dan lingkungan masyarakat memiliki peranan penting bagi tumbuh kembang anak. Sebagai generasi penerus bangsa, sudah sepatutnya anak mendapat perlindungan negara dari tindak segala kekerasan.
                </p>
            </div>
            <div class="item" data-aos="fade-up" data-aos-delay="300">
                <a href="#"  target="_blank" class="my-2">
                    <div class="berita-img">
                        <img data-src="<?= base_url()?>assets/img/portfolio/portfolio-7.jpg" class="owl-lazy img-content-fit h-100" alt="">
                    </div>
                    <div class="date"><span>02/03/2021</span></div>
                    <h3>Perlindungan Anak Terpadu Berbasis Masyarakat Tingkatkan Kualitas Keluarga</h3>
                </a>
                <p class="description">
                    Keluarga dan lingkungan masyarakat memiliki peranan penting bagi tumbuh kembang anak. Sebagai generasi penerus bangsa, sudah sepatutnya anak mendapat perlindungan negara dari tindak segala kekerasan.
                </p>
            </div>
        </div>
        <div class="d-flex justify-content-end mt-3">
            <div class="view-more">
                <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>
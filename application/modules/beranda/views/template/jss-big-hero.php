<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>JSS</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url() ?>asset/v3/img/favicon.png" rel="icon">
  <link href="<?= base_url() ?>asset/v3/img/logo.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url() ?>asset/v3/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/node_modules/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/node_modules/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/vendor/fontawesome/css/all.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/vendor/icofont/icofont.min.css" rel="stylesheet">
  <!-- <link href="<?= base_url() ?>asset/v3/vendor/boxicons/css/boxicons.min.css" rel="stylesheet"> -->
  <link href="<?= base_url() ?>asset/v3/vendor/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?= base_url() ?>asset/v3/vendor/holdon/HoldOn.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css" rel="stylesheet">
  <!-- <link href="<?= base_url() ?>asset/v3/node_modules/animate.css/animate.min.css" rel="stylesheet"> -->

  <!-- Template Main CSS File -->
  <link href="<?= base_url() ?>asset/v3/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">
      <!-- <h1 class="logo me-auto"><a href="#">Techie</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
       <a href="#" class="logo me-auto text-white">
            <img src="<?= base_url() ?>asset/v3/img/logo.png" alt="" class="img-fluid">
            <!-- <span>Smart Service</span> -->
      </a>
       <!-- <a href="#" class="logo d-flex align-items-center">
        <img src="<?= base_url() ?>asset/v3/img/logo.png" alt="">
      </a> -->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
            <li class="active"><a href="#">Beranda</a></li>
            <li><a href="#about">Scan QR</a></li>
            <li><a href="#services">Dokumentasi</a></li>
            <li><a href="#portfolio">FAQ</a></li>
            <li><a href="#team">Tentang</a></li>
            <li><a href="#team">Akun</a></li>
            <li class="drop-down">
                <a href="#" class="nav-profile" data-aos-delay="300">
                    <span>
                        JSS-B2739
                        <img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg" width="25" height="25" class="rounded-circle">
                    </span>
                </a>
                <ul class="profile" role="menu">
                    <li class="name">Drs. SRI WAHYUNI LOREM DOLOR S.Kom, M.kom</li>
                    <li class="jabatan">Petugas Lapangan / Pegawai</li>
                    <li class="dinas">DINAS KOMUNIKASI INFORMATIKA DAN PERSANDIAN</li>
                    <li class="dropdown-divider my-3"></li>
                    <li class="my-2"><a href="#"><i class="fas fa-qrcode"></i> JSS Code</a></li>
                    <li class="my-2"><a href="#"><i class="fas fa-users-cog"></i> Pengaturan Akun</a></li>
                    <li class="text-center mt-3"><a href="#" class="sign-out btn btn-outline-danger">Keluar</a></li>
                </ul>
            </li>
        </ul>
    </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container-fluid" data-aos="fade-up">

        <div class="owl-carousel hero-carousel mx-0 mx-lg-2">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Vaksinasi Sudah Dimulai</h1>
                    <h2>Kota Yogyakarta sudah menyiapkan Program vaksinasi Covid-19 dan akan mulai dilaksanakan pada Jumat 15 Januari 2021</h2>
                    <div class=""><a href="#" class="btn-get-started scrollto">Selengkapnya</a></div>
                </div>
                <div class="col-xl-5 col-lg-6 order-1 order-lg-2 hero-img d-flex justify-content-center" data-aos="zoom-in" data-aos-delay="150">
                    <img src="<?= base_url() ?>asset/v3/img/apd.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Vaksin tidak membuatmu jadi mutan</h1>
                    <h2>We are team of talanted designers making websites with Bootstrap</h2>
                    <div class=""><a href="#" class="btn-get-started scrollto">Selengkapnya</a></div>
                </div>
                <div class="col-xl-5 col-lg-6 order-1 order-lg-2 hero-img d-flex justify-content-center" data-aos="zoom-in" data-aos-delay="150">
                    <img src="<?= base_url() ?>asset/v3/img/walkot.jpg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Vaksin tidak membuatmu jadi mutan</h1>
                    <h2>We are team of talanted designers making websites with Bootstrap</h2>
                    <div class=""><a href="#" class="btn-get-started scrollto">Selengkapnya</a></div>
                </div>
                <div class="col-xl-5 col-lg-6 order-1 order-lg-2 hero-img d-flex justify-content-center" data-aos="zoom-in" data-aos-delay="150">
                    <img src="<?= base_url() ?>asset/v3/img/walkot.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>

  </section><!-- End Hero -->


  <main id="main">
    <section id="fav-app" class="fav-app">
        <div class="fixed-bottom">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="faq-list" data-aos="fade-up">
                        <span data-bs-toggle="collapse" class="collapse" data-bs-target="#fav-app-list">
                            <i class="fas fa-chevron-down line-height icon-close"></i>
                            <i class="fas fa-chevron-up line-height icon-show"></i>
                        </span>
                        <div id="fav-app-list" class="app-box collapse show" data-bs-parent=".faq-list">
                            <div class="dropdown menus">
                                <div class="dropdown-toggle" type="button" id="dropdown-shortcut" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></div>
                                <ul class="dropdown-menu" aria-labelledby="dropdown-shortcut">
                                    <li class="dropdown-item menus-edit">Edit</li>
                                    <li class="dropdown-item menus-done" style="display: none;">Selesai</li>
                                </ul>
                            </div>
                            <div class="aplikasi d-flex justify-content-center pt-2">
                                <div class="item" data-aos-delay="100">
                                    <a href="#">
                                        <img src="<?= base_url() ?>asset/v3/img/logo.png" alt="">
                                        <div class="menu">
                                            <a href="#" class="fav-delete"><i class="fas fa-minus-circle"></i></a>
                                        </div>
                                    </a>
                                </div>
                                <div class="item" data-aos-delay="200">
                                    <a href="#">
                                        <img src="<?= base_url() ?>asset/v3/img/hero-img.png" alt="">
                                        <div class="menu">
                                            <a href="#" class="fav-delete"><i class="fas fa-minus-circle"></i></a>
                                        </div>
                                    </a>
                                </div>
                                <div class="item" data-aos-delay="300">
                                    <a href="#">
                                        <img src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                                        <div class="menu">
                                            <a href="#" class="fav-delete"><i class="fas fa-minus-circle"></i></a>
                                        </div>
                                    </a>
                                </div>
                                <div class="item" data-aos-delay="400">
                                    <a type="button" data-bs-toggle="modal" data-bs-target="#modal-fav-add">
                                        <i class='fas fa-plus-circle'></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade modal-fav-add" id="modal-fav-add" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-appLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="">
                        <h4 class="modal-title">Pintasan Aplikasi</h4>
                        <p class="description">Berbagai macam layanan Jogja Smart Service</p>
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pt-4">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="100">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Gawat Darurat</h4>
                                    <p class="description">Layanan Gawat Darurat dan Kegawatdaruratan Medis (HOTLINE: 119 atau 0274 420118)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="200">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kebakaran</h4>
                                    <p class="description">Layanan Pemadam Kebakaran (HOTLINE: 0274 587101)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="300">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kekerasan (SIKAP)</h4>
                                    <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                                </a>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer border-0">
                </div>
            </div>
        </div>
    </div>

    <!-- ======= Application Section ======= -->
    <section id="app-popular" class="app-popular">
        <div class="container" data-aos="fade-up">

            <div class="border-bottom pb-5">
                <div class="section-title">
                    <p>Layanan top service untuk anda</p>
                </div>

                <div class="mx-2">
                    <div class="owl-carousel app-popular-carousel">
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="200" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="300" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="400" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="500" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="600" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="700" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                        <a href="#" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="800" data-bs-toggle="tooltip" data-placement="top" title="Aplikasi Abc" >
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/app.png" alt="">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Application Section -->


    <!-- ======= Smart Services Section ======= -->
    <section id="smart-service" class="smart-service pb-5">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Smart Service</h2>
                <p>Jelajahi aplikasi dan pelayanan di Kota Yogyakarta</p>
            </div>
            <div class="mb-4">
                <form action="#" method="post" role="form" class="php-email-form">
                    <div class="form-group">
                        <input type="text" class="form-control text-center form-rounded" name="keyword" id="keyword" placeholder="Telusuri aplikasimu disini..." />
                        <div class="validate"></div>
                    </div>
                </form>
            </div>

            <div class="" data-aos="fade-up">
                <div class="panel-category owl-carousel" id="panel-category">

                    <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="100">
                        <div class="box-category">
                            <div class="img-box"><img data-src="<?= base_url() ?>asset/v3/img/category/Kategori eGovernment.png" class="owl-lazy" alt=""></div>
                            <h4 class="title">eGovernment</h4>
                            <hr>
                            <p class="description">Merupakan layanan informasi dan pelayanan pemerintah kepada masyarakat</p>
                        </div>
                    </div>
                    <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="200">
                        <div class="box-category">
                            <div class="img-box"><img data-src="<?= base_url() ?>asset/v3/img/category/Kategori Informasi Pengaduan.png" class="owl-lazy" alt=""></div>
                            <h4 class="title">Pengaduan</h4>
                            <hr>
                            <p class="description">Merupakan layanan ticketing yang ditujukan untuk menerima pelaporan dari masyarakat</p>
                        </div>
                    </div>
                    <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="300">
                        <div class="box-category">
                            <div class="img-box"><img data-src="<?= base_url() ?>asset/v3/img/category/Kategori Kedaruratan.png" class="owl-lazy" alt=""></div>
                            <h4 class="title">Kedaruratan</h4>
                            <hr>
                            <p class="description">Merupakan layanan yang diberikan untuk mendapatkan akses cepat dalam kondisi darurat</p>
                        </div>
                    </div>
                    <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="400">
                        <div class="box-category">
                            <div class="img-box"><img data-src="<?= base_url() ?>asset/v3/img/category/Kategori Layanan Umum.png" class="owl-lazy" alt=""></div>
                            <h4 class="title">Layanan Umum</h4>
                            <hr>
                            <p class="description">Merupakan layanan informasi dan pelayanan pemerintah kepada masyarakat</p>
                        </div>
                    </div>
                    <div class="item" type="button" data-bs-toggle="modal" data-bs-target="#modal-app" data-aos="fade-up" data-aos-delay="500">
                        <div class="box-category">
                            <div class="img-box"><img data-src="<?= base_url() ?>asset/v3/img/category/Kategori Layanan Umum.png" class="owl-lazy" alt=""></div>
                            <h4 class="title">Layanan Umum</h4>
                            <hr>
                            <p class="description">Merupakan layanan informasi dan pelayanan pemerintah kepada masyarakat</p>
                        </div>
                    </div>
                </div>
                <div class="row panel-search-app" id="panel-search-app" style="display: none;">
                    <div class="col-lg-3 col-md-6" data-aos="fade-up">
                        <div class="logo-box">
                            <div class="img-app"><img src="<?= base_url() ?>asset/v3/img/app.png" class="smart-service-logo" alt=""></div>
                            <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                            <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <div class="logo-box">
                            <div class="img-app"><img src="<?= base_url() ?>asset/v3/img/app.png" class="smart-service-logo" alt=""></div>
                            <h4 class="title"><a href="">Dolor Sitema</a></h4>
                            <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                        <div class="logo-box">
                            <div class="img-app"><img src="<?= base_url() ?>asset/v3/img/app.png" class="smart-service-logo" alt=""></div>
                            <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                            <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="logo-box">
                            <div class="img-app"><img src="<?= base_url() ?>asset/v3/img/app.png" class="smart-service-logo" alt=""></div>
                            <h4 class="title"><a href="">Magni Dolores</a></h4>
                            <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Smart Services Section -->

    <!-- Modal App -->
    <div class="modal fade modal-app" id="modal-app" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-appLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <img src="<?= base_url() ?>asset/v3/img/category/Kategori Kedaruratan.png" class="img-fluid img-header" alt="">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pt-0">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 mx-auto text-center">
                                <h4 class="modal-title" id="modal-appLabel">Kedaruratan</h4>
                                <p class="description">Berbagai macam layanan kegawatdaruratan</p>
                                <hr class="divider">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="100">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Gawat Darurat</h4>
                                    <p class="description">Layanan Gawat Darurat dan Kegawatdaruratan Medis (HOTLINE: 119 atau 0274 420118)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="200">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kebakaran</h4>
                                    <p class="description">Layanan Pemadam Kebakaran (HOTLINE: 0274 587101)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="300">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kekerasan (SIKAP)</h4>
                                    <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="300">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kekerasan (SIKAP)</h4>
                                    <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="300">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kekerasan (SIKAP)</h4>
                                    <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                                </a>
                            </div>
                            <div class="col-md-6 mx-auto mt-2" data-aos="fade-up" data-aos-delay="300">
                                <a href="#" target="_blank" class="box-category">
                                    <div class="img-box"><img src="<?= base_url() ?>asset/v3/img/app.png" class="" alt=""></div>
                                    <h4 class="title">Kekerasan (SIKAP)</h4>
                                    <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                                </a>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer border-0">
                </div>
            </div>
        </div>
    </div>


    <!-- ======= Wisata Budaya Section ======= -->
    <section id="wisata-budaya" class="wisata-budaya">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Pariwisata & Budaya</h2>
                <p>Pantau event-event menarik di Kota Jogja</p>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" data-aos="fade-up" data-aos-delay="100">
                    <div class="artikel">
                        <div class="big-thumb">
                            <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="artikel-info">
                            <div class="artikel-info-content">
                                <a href="#"><h4>Udang Level Special Khas Bu Rudy Tabuty Numero Uno</h4></a>
                                <p>Perayaan di masa Pandemi Covid-19 ini menjadi momentum bagi warga Kota Yogyakarta untuk menyatukan tekad dan salingr</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="row wisata-artikel owl-carousel" id="wisata-artikel" data-aos="fade-up" data-aos-delay="200">

                        <div class="col-sm-12">
                            <div class="item">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-12">
                                        <div class="artikel">
                                            <div class="small-thumb">
                                                <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12">
                                        <div class="artikel-info-content">
                                            <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                            <a href="#"><h6>Pelantikan Pengurus PBVSI 2020-2024 Dikukuhkan Kota Yogyakarta Kota Yogyakarta</h6></a>
                                        </div>
                                        <div class="artikel-info">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-12">
                                        <div class="artikel">
                                            <div class="small-thumb">
                                                <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12">
                                        <div class="artikel-info-content">
                                            <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                            <a href="#"><h6>Kota Yogyakarta Kota Yogyakarta</h6></a>
                                        </div>
                                        <div class="artikel-info">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="item">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-12">
                                        <div class="artikel">
                                            <div class="small-thumb">
                                                <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12">
                                        <div class="artikel-info-content">
                                            <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                            <a href="#"><h6>Kota Yogyakarta Kota Yogyakarta</h6></a>
                                        </div>
                                        <div class="artikel-info">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-12">
                                        <div class="artikel">
                                            <div class="small-thumb">
                                                <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12">
                                        <div class="artikel-info-content">
                                            <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                            <a href="#"><h6>Kota Yogyakarta Kota Yogyakarta</h6></a>
                                        </div>
                                        <div class="artikel-info">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Wisata Budaya Section -->

    <!-- ======= Promotion Section ======= -->
    <section id="promotion" class="promotion">
        <div class="container">

            <div class="section-title">
                <h2>Promosi</h2>
                <p>Lihat berbagai macam penawaran menarik disini.</p>
            </div>

            <div class="row">

                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="event d-flex align-items-start" data-aos="fade-up" data-aos-delay="100">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="pic"><img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-1.jpg" class="img-fluid" alt=""></div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="event-info py-4 py-md-0">
                                    <h4>TerPIZZA HEBOH!</h4>
                                    <ul>
                                        <li><i class="fas fa-map-marker-alt"></i> Sekar Kedhaton Resto</li>
                                        <li><i class="fas fa-stopwatch"></i> 2 Hari 4 Jam</li>
                                    </ul>
                                    <p>Dapatkan promo tahun baru kamu jadi makin seru dengan pilihan paket pizza paling hemat</p>
                                </div>
                                <div class="get-it">
                                    <a href="#">Dapatkan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="event d-flex align-items-start" data-aos="fade-up" data-aos-delay="200">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="pic"><img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-2.jpg" class="img-fluid" alt=""></div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="event-info py-4 py-md-0">
                                    <h4>Diskon GEREGETAN sampai dengan 55% - 100%</h4>
                                    <ul>
                                        <li><i class='fas fa-map-marker-alt'></i> Sekar Kedhaton Resto</li>
                                        <li><i class="fas fa-stopwatch"></i> 2 Hari 4 Jam</li>
                                    </ul>
                                    <p>Pesan akomodasi dengan privasi extra untuk pengalaman liburan tak terlupakan</p>
                                </div>
                                <div class="get-it">
                                    <a href="#">Dapatkan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-end mt-5">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section><!-- End Promotion Section -->

    <!-- ======= Pengaduan Section ======= -->
    <section id="pengaduan" class="pengaduan">
        <div class="container" data-aos="fade-up" data-aos-delay="100">

            <div class="section-title">
                <h2>Pengaduan</h2>
                <p>Pantau aduan masyarakat</p>
            </div>

            <div class="row">

                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                    <article class="entry">
                        <div class="entry-img">
                            <img src="<?= base_url() ?>asset/v3/img/pengaduan.jpg" alt="" class="img-fluid">
                            <div class="label bg-done">
                                <span>Selesai</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>


                    </article><!-- End blog entry -->
                </div>
                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="<?= base_url() ?>asset/v3/img/pengaduan-2.jpg" alt="" class="img-fluid">
                            <div class="label bg-onprogress">
                                <span>Dikerjakan</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>

                    </article>
                </div>
                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-3.jpg" alt="" class="img-fluid">
                            <div class="label bg-onprogress">
                                <span>Dikerjakan</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>

                    </article>
                </div>
                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="500">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-4.jpg" alt="" class="img-fluid">
                            <div class="label bg-onprogress">
                                <span>Dikerjakan</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>

                    </article>
                </div>

            </div>

            <div class="d-flex justify-content-center">
                <div class="add-pengaduan">
                    <a href="#" target="_blank" class="">Tambahkan</a>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section><!-- End Pengaduan Section -->

    <!-- ======= UMKM Produk ======= -->
    <section id="umkm-produk" class="umkm-produk">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Produk Jogja</h2>
                <p>Lihat produk dari usaha kecil menengah.</p>
            </div>

            <div class="mx-2">
                <div class="owl-carousel umkm-produk-carousel">
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="100">
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Udang Level Special Khas Bu Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="200">
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Special Khas Bu Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="300">
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="produk shadow-sm" data-aos="fade-up" data-aos-delay="400">
                            <img class="owl-lazy" data-src="<?= base_url() ?>asset/v3/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
                            <div class="produk-info">
                                <div class="produk-info-content">
                                    <h4>Peyeks Level Special Khas Bu Rudy Tabuty</h4>
                                </div>
                                <div class="social">
                                    <a href="#" class="read-more">Beli</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section><!-- End Pengaduan Section -->


    <section id="jsstv" class="jsstv section-bg">
        <div class="container">
            <div class="section-title">
                <h2>JSS TV</h2>
                <p>Update informasi Kota Jogja dari siaran</p>
            </div>

            <div class="row">

                <div class="col-xl-8 col-lg-8 d-flex justify-content-center align-items-stretch position-relative mb-4" data-aos="fade-right">
                    <div class="big-video-box">
                        <img src="https://img.youtube.com/vi/mT9S3_-i_Ds/maxresdefault.jpg" alt="" class="img-fluid">
                        <a href="https://www.youtube.com/watch?v=mT9S3_-i_Ds" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 icon-boxes d-flex flex-column justify-content-center px-lg-3">
                    <div class="row" data-aos="fade-left" data-aos-delay="200">
                        <div class="col-lg-12 col-md-12 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                            <div class="tv-entry">
                                <div class="small-thumb">
                                    <img src="https://img.youtube.com/vi/luGZs0dbylw/maxresdefault.jpg" alt="" class="img-fluid">
                                    <a href="https://www.youtube.com/watch?v=luGZs0dbylw" class="venobox small-play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                                </div>
                                <h3 class="tv-title">
                                    <a href="#">Wayang Jogja Night Carnival (WJNC) #5 Kota Jogja</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row" data-aos="fade-left" data-aos-delay="200">
                        <div class="col-lg-12  col-md-12 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                            <div class="tv-entry">
                                <div class="small-thumb">
                                    <img src="https://img.youtube.com/vi/Xv2nASuVvzw/maxresdefault.jpg" alt="" class="img-fluid">
                                    <a href="https://www.youtube.com/watch?v=Xv2nASuVvzw" class="venobox small-play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                                </div>
                                <h3 class="tv-title">
                                    <a href="#">Focus Group Discussion (Sinergi Pengembangan Inovasi)</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-end mt-4">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section><!-- End jogjaTV -->

</main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-6 footer-contact">
                    <a href="#" class="footer-logo text-white d-flex">
                        <img src="<?= base_url() ?>asset/v3/img/logo.png" alt="" class="img-fluid">
                        <h3>Jogja Smart Service</h3>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Apa sih JSS itu ?</h4>
                    <p>Jogja Smart Service adalah Balaikota Virtual atau Portal maya Pemerintah Kota Yogyakarta dalam rangka memberikan layanan langsung kepada semua masyarakat di Kota Yogyakarta.</p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Satu Pintu  </h4>
                    <p>Daftar layanan Pemerintah Kota yang dapat diakses langsung oleh masyarakat dengan mengedepankan pelayanan mandiri (Swalayan).</p>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <div class="text-center">
                        <a href="#" target="_blank" class="text-white">
                            <img src="<?= base_url() ?>asset/v3/img/ios.png" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="text-center">
                        <a href="#" target="_blank" class="text-white">
                            <img src="<?= base_url() ?>asset/v3/img/gplay.png" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="social-links text-center text-md-right pt-3 pt-md-2">
                        <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">

        <div class="copyright-wrap d-md-flex py-4">
            <div class="me-md-auto text-center text-md-start">
                <div class="copyright">
                    <strong><span>Jogja Smart Service.</span></strong>
                </div>
                <div class="credits">
                    <!-- All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: https://bootstrapmade.com/license/ -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/techie-free-skin-bootstrap-3/ -->
                    &copy; 2021 <a href="https://jogjakota.go.id/">Jogjakota.go.id</a>. All right reserved. Hak Cipta <a href="#">Diskominfo</a>
                </div>
            </div>
            <!-- <div class="social-links text-center text-md-right pt-3 pt-md-0">
                 <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
            </div> -->
        </div>

    </div>
</footer><!-- End Footer -->

  <!-- <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a> -->
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="<?= base_url() ?>asset/v3/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js"></script> -->
  <!-- <script src="<?= base_url() ?>asset/v3/node_modules/bootstrap/dist/js/bootstrap.min.js"></script> -->
  <!-- <script src="<?= base_url() ?>asset/v3/node_modules/@popperjs/core/dist/umd/popper.minjs"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.js"></script> -->
  <!-- <script src="<?= base_url() ?>asset/v3/vendor/php-email-form/validate.js"></script> -->
  <script src="<?= base_url() ?>asset/v3/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/counterup/counterup.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/venobox/venobox.min.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/aos/aos.js"></script>
  <script src="<?= base_url() ?>asset/v3/vendor/holdon/HoldOn.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url() ?>asset/v3/js/main.js"></script> 
  <script src="<?= base_url() ?>asset/v3/js/app.js"></script> 

</body>

</html>
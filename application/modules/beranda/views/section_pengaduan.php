<!-- ======= Pengaduan Section ======= -->
    <section id="pengaduan" class="pengaduan">
        <div class="container" data-aos="fade-up" data-aos-delay="100">

            <div class="section-title">
                <h2>Pengaduan</h2>
                <p>Pantau aduan masyarakat</p>
            </div>

            <div class="row">

                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                    <article class="entry">
                        <div class="entry-img">
                            <img src="<?= base_url()?>assets/img/pengaduan.jpg" alt="" class="img-fluid img-content-fit h-100">
                            <div class="label bg-done">
                                <span>Selesai</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>


                    </article><!-- End blog entry -->
                </div>
                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="<?= base_url()?>assets/img/pengaduan-2.jpg" alt="" class="img-fluid img-content-fit h-100">
                            <div class="label bg-onprogress">
                                <span>Dikerjakan</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>

                    </article>
                </div>
                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="<?= base_url()?>assets/img/portfolio/portfolio-3.jpg" alt="" class="img-fluid img-content-fit h-100">
                            <div class="label bg-onprogress">
                                <span>Dikerjakan</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>

                    </article>
                </div>
                <div class="col-lg-6  col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="500">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="<?= base_url()?>assets/img/portfolio/portfolio-4.jpg" alt="" class="img-fluid img-content-fit h-100">
                            <div class="label bg-onprogress">
                                <span>Dikerjakan</span>
                            </div>
                        </div>
                        <h2 class="entry-title">
                            <a href="#">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
                        </h2>
                        <div class="entry-content">
                            <p>
                                Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta zena prista maraeda talan mas indera.
                            </p>
                        </div>

                    </article>
                </div>

            </div>

            <div class="d-flex justify-content-center">
                <div class="add-pengaduan">
                    <a href="#" target="_blank" class="">Tambahkan</a>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <div class="view-more">
                    <a href="#" class="py-1">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section><!-- End Pengaduan Section -->

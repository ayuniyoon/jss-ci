<!-- ======= Wisata Budaya Section ======= -->
<section id="wisata-budaya" class="wisata-budaya">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Pariwisata & Budaya</h2>
            <p>Pantau event-event menarik di Kota Jogja</p>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" data-aos="fade-up" data-aos-delay="100">
                <div class="artikel">
                    <div class="big-thumb">
                        <img src="<?= base_url()?>assets/img/portfolio/portfolio-2.jpg" class="img-fluid img-content-fit h-100" alt="">
                    </div>
                    <div class="artikel-info">
                        <div class="artikel-info-content">
                            <a href="#"><h4>Udang Level Special Khas Bu Rudy Tabuty Numero Uno</h4></a>
                            <p>Perayaan di masa Pandemi Covid-19 ini menjadi momentum bagi warga Kota Yogyakarta untuk menyatukan tekad dan salingr</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="row wisata-artikel owl-carousel" id="wisata-artikel" data-aos="fade-up" data-aos-delay="200">

                    <div class="col-sm-12">
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="artikel">
                                        <div class="small-thumb">
                                            <img src="<?= base_url()?>assets/img/portfolio/portfolio-1.jpg" class="img-fluid img-content-fit h-100" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12">
                                    <div class="artikel-info-content">
                                        <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                        <a href="#"><h6>Pelantikan Pengurus PBVSI 2020-2024 Dikukuhkan Kota Yogyakarta Kota Yogyakarta</h6></a>
                                    </div>
                                    <div class="artikel-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="artikel">
                                        <div class="small-thumb">
                                            <img src="<?= base_url()?>assets/img/portfolio/portfolio-2.jpg" class="img-fluid img-content-fit h-100" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12">
                                    <div class="artikel-info-content">
                                        <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                        <a href="#"><h6>Kota Yogyakarta Kota Yogyakarta</h6></a>
                                    </div>
                                    <div class="artikel-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="artikel">
                                        <div class="small-thumb">
                                            <img src="<?= base_url()?>assets/img/portfolio/portfolio-3.jpg" class="img-fluid img-content-fit h-100" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12">
                                    <div class="artikel-info-content">
                                        <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                        <a href="#"><h6>Kota Yogyakarta Kota Yogyakarta</h6></a>
                                    </div>
                                    <div class="artikel-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="artikel">
                                        <div class="small-thumb">
                                            <img src="<?= base_url()?>assets/img/portfolio/portfolio-4.jpg" class="img-fluid img-content-fit h-100" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12">
                                    <div class="artikel-info-content">
                                        <span><i class="far fa-calendar-alt"></i> Kamis 01/10/2020 22:04 WIB</span>
                                        <a href="#"><h6>Kota Yogyakarta Kota Yogyakarta</h6></a>
                                    </div>
                                    <div class="artikel-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Wisata Budaya Section -->
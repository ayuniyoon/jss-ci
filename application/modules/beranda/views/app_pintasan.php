<section id="fav-app" class="fav-app">
    <div class="fixed-bottom">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="faq-list" data-aos="fade-up">
                    <span data-bs-toggle="collapse" class="collapse" data-bs-target="#fav-app-list">
                        <i class="fas fa-chevron-down line-height icon-close"></i>
                        <i class="fas fa-chevron-up line-height icon-show"></i>
                    </span>
                    <div id="fav-app-list" class="app-box collapse show" data-bs-parent=".faq-list">
                        <div class="dropdown menus">
                            <div class="dropdown-toggle" type="button" id="dropdown-shortcut" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></div>
                            <ul class="dropdown-menu" aria-labelledby="dropdown-shortcut">
                                <li class="dropdown-item menus-edit">Edit</li>
                                <li class="dropdown-item menus-done" style="display: none;">Selesai</li>
                            </ul>
                        </div>
                        <div class="aplikasi d-flex justify-content-center pt-2">
                            <div class="item" data-aos-delay="100">
                                <a href="#">
                                    <img src="<?= base_url()?>assets/img/logo.png" alt="">
                                    <div class="menu">
                                        <a href="#" class="fav-delete"><i class="fas fa-minus-circle"></i></a>
                                    </div>
                                </a>
                            </div>
                            <div class="item" data-aos-delay="200">
                                <a href="#">
                                    <img src="<?= base_url()?>assets/img/app.png" alt="">
                                    <div class="menu">
                                        <a href="#" class="fav-delete"><i class="fas fa-minus-circle"></i></a>
                                    </div>
                                </a>
                            </div>
                            <div class="item" data-aos-delay="300">
                                <a href="#">
                                    <img src="<?= base_url()?>assets/img/app.png" alt="">
                                    <div class="menu">
                                        <a href="#" class="fav-delete"><i class="fas fa-minus-circle"></i></a>
                                    </div>
                                </a>
                            </div>
                            <div class="item" data-aos-delay="400">
                                <a type="button" data-bs-toggle="modal" data-bs-target="#modal-fav-add">
                                    <i class='fas fa-plus-circle'></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal App -->
<div class="modal fade modal-app" id="modal-fav-add" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-appLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header border-0">
                <img src="<?= base_url()?>assets/img/category/Kategori Kedaruratan.png" class="img-fluid img-header" alt="">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pt-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mx-auto text-center">
                            <h4 class="modal-title" id="modal-appLabel">Pintasan Aplikasi</h4>
                            <p class="description">Berbagai macam layanan Jogja Smart Service</p>
                            <hr class="divider">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <form action="#" method="post" role="form" class="">
                            <div class="form-group">
                                <input type="text" class="form-control text-center form-rounded" name="modal-keyword" id="modal-keyword" placeholder="Telusuri aplikasimu disini..." />
                                <div class="validate"></div>
                            </div>
                        </form>
                    </div>
                    <div class="row mb-3" id="services">
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="100">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Gawat Darurat</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Gawat Darurat dan Kegawatdaruratan Medis (HOTLINE: 119 atau 0274 420118)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="200">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kebakaran</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pemadam Kebakaran (HOTLINE: 0274 587101)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                        <div class="col-md-4 mx-auto mt-2 text-center" data-aos="fade-up" data-aos-delay="300">
                            <a href="#" target="_blank" class="box-category">
                                <div class="img-box"><img src="<?= base_url()?>assets/img/app.png" class="" alt=""></div>
                                <h4 class="title">Kekerasan (SIKAP)</h4>
                            </a>
                            <div class="panduan">
                                <a href="#">deskripsi</a> | 
                                <a href="#">panduan</a> | 
                                <a href="#">video</a>
                            </div>
                            <p class="description">Layanan Pengaduan Kekerasan terhadap Anak dan Perempuan berbasis Gender (HOTLINE: 0274-514419)</p>
                        </div>
                    </div>                        
                </div>
            </div>
            <div class="modal-footer border-0">
            </div>
        </div>
    </div>
</div>
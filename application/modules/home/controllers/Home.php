<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Home extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->module('layout');
	}

	public function index()
	{
		$this->layout->header();
		$this->layout->navbar();
		$this->layout->heroarea();
		$this->load->view('beranda/app_smart_service');
		$this->load->view('beranda/section_berita');
		$this->load->view('beranda/section_wisata_budaya');
		$this->load->view('beranda/section_promosi');
		$this->load->view('beranda/section_pengaduan');
		$this->load->view('beranda/section_produk_umkm');
		$this->load->view('beranda/section_jss_tv');
		$this->load->view('section_modal_login');
		$this->load->view('section_modal_register');
		$this->load->view('section_modal_unduh');
		$this->load->view('section_modal_forgot_pwd');
		$this->load->view('section_modal_reactivated');
		$this->load->view('section_homepage_js');
		$this->layout->footer();
	}

}
<script type="text/javascript">

    var base_url = "<?=base_url('home');?>";

    function login()
    {
        window.location.href = base_url + "?login=true";
    }
    function register()
    {
        window.location.href = base_url + "?register=true";
    }
    function unduh()
    {
        window.location.href = base_url + "?unduh=true";
    }
    function forgot()
    {
        window.location.href = base_url + "?forgot=true";
    }
    function reactivate()
    {
        window.location.href = base_url + "?reactivate=true";
    }

    $(document).ready(function(){
        <?php if (!empty($_GET['login']) && $_GET['login'] == "true"): ?>
            var myModal = new bootstrap.Modal(document.getElementById("modalLogin"), {});
            myModal.show();
        <?php endif ?>

        <?php if (!empty($_GET['register']) && $_GET['register'] == "true"): ?>
            var myModal = new bootstrap.Modal(document.getElementById("modalDaftar"), {});
            myModal.show();
        <?php endif ?>
        
         <?php if (!empty($_GET['unduh']) && $_GET['unduh'] == "true"): ?>
            var myModal = new bootstrap.Modal(document.getElementById("modalUnduh"), {});
            myModal.show();
        <?php endif ?>

        <?php if (!empty($_GET['forgot']) && $_GET['forgot'] == "true"): ?>
            var myModal = new bootstrap.Modal(document.getElementById("modalForgot"), {});
            myModal.show();
        <?php endif ?>

        <?php if (!empty($_GET['reactivate']) && $_GET['reactivate'] == "true"): ?>
            var myModal = new bootstrap.Modal(document.getElementById("modalReactivate"), {});
            myModal.show();
        <?php endif ?>
    });

</script>
<div class="modal fade" id="modalLogin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="mb-4">
                    <ul class="nav justify-content-center login-menu">
                        <li class="nav-item">
                            <a class="px-2 active border-bottom" aria-current="page" onclick="login()">MASUK</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" onclick="register()">DAFTAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" onclick="unduh()">UNDUH APK</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" href="<?=base_url('panduan');?>">PANDUAN</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" href="<?=base_url('faq');?>">FAQ</a>
                        </li>
                    </ul>
                </div>
                <div class="text-center mb-4">
                    <img class="img-responsive" src="<?=base_url().'assets/img/logo.png';?>" onerror="<?=base_url() . 'assets/img/logo.png';?>" alt="User Icon" width="100" />
                </div>
                <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="message"></div>
                </div>
                <form id="form-login" role="form" action="<?=base_url('auth_v2/signin');?>" method="POST">
                    <div class="input-group mb-2">
                        <input type="text" id="login" class="form-control" name="akun" placeholder="Username" maxlength="40" autofocus="">
                        <span class="input-group-text" id="basic-addon1"><i class="far fa-user px-1"></i></span>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="password" maxlength="25" autocomplete="false" placeholder="Kata Kunci">
                        <span class="input-group-text" id="basic-addon1"><i class="far fa-eye pd-sm"></i></span>
                    </div>
                    <div class="mb-2">
                        <button type="submit" class="form-control btn btn-sm btn-info text-white" id="btn-masuk"><i class="fas fa-sign-in-alt"></i> Masuk !</button>
                    </div>
                </form>

                <div class="mb-2" style="display: none;">
                    <button type="button" disabled="disabled" class="w-100 btn btn-sm btn-danger" id="btn-lock">Mohon tunggu 10 detik untuk masuk</button>
                </div>
                <div class="mb-2">
                    <button type="button" class="w-100 btn btn-sm btn-danger" data-bs-dismiss="modal">Tutup</button>
                </div>
                <div class="text-center mb-2">
                    <small for="aktivasi" class="fw-lighter">Mengalami kendala dengan Aplikasi JSS ?</small>
                </div>
                <div class="text-center mb-2">
                    <a href="//wa.me/628995492195" target="_blank" class="btn btn-sm btn-success text-white"><i class="fab fa-whatsapp text-white"></i> Chat CS JSS</a>
                </div>
            </div>
            <div class="modal-footer d-flex bd-highlight">
                <a onclick="forgot()" class="me-auto bd-highlight">Lupa Password</a>
                <a onclick="reactivate()" class="bd-highlight">Aktivasi Ulang</a>
            </div>
        </div>
    </div>
</div>
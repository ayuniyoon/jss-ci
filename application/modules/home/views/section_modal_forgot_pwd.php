<div class="modal fade" id="modalForgot" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center mt-2 mb-4">
                    <img class="img-responsive" src="<?=base_url().'assets/img/logo.png';?>" onerror="<?=base_url() . 'assets/img/logo.png';?>" alt="User Icon" width="100" />
                </div>
                <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="message"></div>
                </div>

                <form id="form-login" role="form" action="<?=base_url('auth_v2/signin');?>" method="POST">

                    <?php if ($this->session->flashdata('status') == 'true'): ?>
                        <div class="">
                            <div class="input-group mb-2">
                                <label for="email">Pilih Metode Verifikasi</label>
                                <p><small>Pilih salah satu metode di bawah ini untuk mendapatkan kode verifikasi</small></p>
                            </div>
                            <div class="splash-box cotp__box--change-method">
                                <?php if ($this->session->flashdata('data')['no_wa_mask']): ?>
                                    <a href="<?php echo base_url('akunv2/lupa_sandi/verifikasi/wa') ?>" target="_blank">
                                        <div class="cotp__change-list">
                                            <div class="cotp__change-list-left">
                                                <span><i class="fa fa-whatsapp" style="font-size: 32px;"></i></span>
                                            </div>
                                            <div class="text-black54 cotp__change-list-right">
                                                <h3 class="lh-20 inline-block text-black7">Ganti via <span class="fw-600">Whatsapp</span></h3>
                                                <p>ke <span class="inline-block"><?=$this->session->flashdata('data')['no_wa_mask'] ?></span></p>
                                            </div>
                                            <span class="cotp__change-btn"></span>
                                        </div>
                                    </a>
                                <?php endif ?>
                                <?php if (!empty($this->session->flashdata('data')['email_mask'])): ?>
                                    <a href="<?php echo base_url('akunv2/lupa_sandi/verifikasi/email') ?>">
                                        <div id="" class="cotp__change-list">
                                            <div class="cotp__change-list-left">
                                                <span><i class="fa fa-envelope" style="font-size: 32px;"></i></span>
                                            </div>
                                            <div class="text-black54 cotp__change-list-right">
                                                <h3 class="lh-20 inline-block text-black7">Ganti via <span class="fw-600">Email</span></h3>
                                                <p>ke <span class="inline-block"><?=$this->session->flashdata('data')['email_mask'] ?></span></p>
                                            </div>
                                            <span class="cotp__change-btn"> </span>
                                        </div>
                                    </a>
                                <?php endif ?>
                            </div>
                        </div>

                        <?php elseif ($this->session->flashdata('status') == 'verify'): ?>

                            <div class="panel panel-default">
                                <div class="panel-body text-center">
                                    <div class="input-group mb-2">
                                        <label for="email">Masukkan Kode Verifikasi</label>
                                    </div>
                                    <form class="verify-forgot" id="verify-forgot" method="post" action="<?= base_url('akunv2/lupa_sandi/verify')?>">
                                        <input class="codeverify" type="text" name="ip1" maxLength="1" size="1" min="0" required="required" autofocus="autofocus" />
                                        <input class="codeverify" type="text" name="ip2" maxLength="1" size="1" min="0" required="required" autofocus="autofocus" />
                                        <input class="codeverify" type="text" name="ip3" maxLength="1" size="1" min="0" required="required" autofocus="autofocus" />
                                        <input class="codeverify" type="text" name="ip4" maxLength="1" size="1" min="0" required="required" autofocus="autofocus" />
                                        <input class="codeverify" type="text" name="ip5" maxLength="1" size="1" min="0" required="required" autofocus="autofocus" />
                                        <input class="codeverify" type="text" name="ip6" maxLength="1" size="1" min="0" required="required" autofocus="autofocus" />
                                        <br/><br/>
                                        <button type="submit" class="btn btn-primary btn-embossed">Verifikasi</button>
                                        <br/><br/>
                                    </form>
                                    <div id="countdown"> </div>
                                </div>
                            </div>

                            <?php elseif ($this->session->flashdata('status') == 'dochange'): ?>

                                <form  role="form" id="dochangepwd" action="<?=base_url('akunv2/lupa_sandi/dochange');?>" method="POST">
                                    <div class="">
                                        <div class="input-group mb-2">
                                            <label for="email">Lupa Password</label>
                                        </div>
                                        <div class="input-group mb-2">
                                            <input required type="password" name="forgot_new_password" maxlength="20" class="form-control fadeIn first text-center" placeholder="Kata Kunci Baru" autofocus>
                                        </div>
                                        <div class="input-group mb-2">
                                            <input required type="password" name="forgot_passconf" maxlength="20" class="form-control fadeIn first text-center" placeholder="Konfirmasi Kata Kunci" autofocus>
                                        </div>
                                        <div class="input-group mb-2">
                                            <p><small>Catatan : Dilarang memberitahu tahu password anda ke orang lain ataupun orang yg mengatasnamakan sebagai operator JSS.</small></p>
                                        </div>
                                        <div class="input-group mb-2">
                                            <button type="submit" class="btn btn-info btn-block fadeIn second" value="Ubah">Ubah Sekarang !</button>
                                        </div>
                                    </div>
                                </form>

                            <?php else: ?>
                                <form role="form" action="<?=base_url('akunv2/lupa_sandi');?>" method="POST">
                                    <div class="text-center mb-3">
                                        <label class="fw-bold">Lupa Password</label><br>
                                        <small for="aktivasi" class="fw-lighter">Silahkan masukkan Nomor KTP atau Nomor Whatsapp anda yang terdaftar.</small>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input required type="text" id="forgot_password" name="forgot_password" class="form-control fadeIn first text-center angka" maxlength="16" placeholder="Nomor KTP atau Nomor Whatsapp" autofocus>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-sm w-100 text-white" value="Kirim"><i class="far fa-paper-plane"></i> KIRIM</button>
                                    </div>
                                </form>
                            <?php endif ?>
                        </form>
                    </div>
                    <div class="modal-footer d-flex bd-highlight">
                        <a class="me-auto bd-highlight" onclick="login()" >Kembali</a>
                        <a class="bd-highlight" data-bs-dismiss="modal">Tutup</a>
                    </div>
        </div>
    </div>
</div>
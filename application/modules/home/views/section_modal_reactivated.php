<div class="modal fade" id="modalReactivate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center mt-2 mb-4">
                    <img class="img-responsive" src="<?=base_url().'assets/img/logo.png';?>" onerror="<?=base_url() . 'assets/img/logo.png';?>" alt="User Icon" width="100" />
                </div>
                <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="message"></div>
                </div>

                <?php if ($this->session->flashdata('status') == 'true'): ?>
                    <div class="">
                        <div class="text-center mb-3">
                            <label class="fw-bold">Pilih Metode Aktivasi Akun</label><br>
                            <small for="aktivasi" class="fw-lighter">Pilih salah satu metode di bawah ini untuk mendapatkan Link Aktivasi.</small>
                        </div>
                        <div class="splash-box cotp__box--change-method">
                            <?php if ($this->session->flashdata('data')['no_wa_mask']): ?>
                                <a href="<?php echo base_url('akunv2/aktivasi/verifikasi/wa') ?>" target="_blank">
                                    <div class="cotp__change-list">
                                        <div class="cotp__change-list-left">
                                            <span><i class="fa fa-whatsapp" style="font-size: 32px;"></i></span>
                                        </div>
                                        <div class="text-black54 cotp__change-list-right">
                                            <h3 class="lh-20 inline-block text-black7">Ganti via <span class="fw-600">Whatsapp</span></h3>
                                            <p>ke <span class="inline-block"><?=$this->session->flashdata('data')['no_wa_mask'] ?></span></p>
                                        </div>
                                        <span class="cotp__change-btn"></span>
                                    </div>
                                </a>
                            <?php endif ?>
                            <?php if (!empty($this->session->flashdata('data')['email_mask'])): ?>
                                <a href="<?php echo base_url('akunv2/aktivasi/verifikasi/email') ?>">
                                    <div id="" class="cotp__change-list">
                                        <div class="cotp__change-list-left">
                                            <span><i class="fa fa-envelope" style="font-size: 32px;"></i></span>
                                        </div>
                                        <div class="text-black54 cotp__change-list-right">
                                            <h3 class="lh-20 inline-block text-black7">Ganti via <span class="fw-600">Email</span></h3>
                                            <p>ke <span class="inline-block"><?=$this->session->flashdata('data')['email_mask'] ?></span></p>
                                        </div>
                                        <span class="cotp__change-btn"> </span>
                                    </div>
                                </a>
                            <?php endif ?>
                        </div>
                    </div>

                    <?php elseif ($this->session->flashdata('status') == 'verify'): ?>
                        <div class="text-center mb-3">
                            <label class="fw-bold">Aktivasi Ulang</label><br>
                        </div>
                        <?php if ($this->session->flashdata('akun')): ?>
                            <div class="input-group mb-3">
                                <div class="alert alert-<?= $this->session->flashdata('alert') ?> mx-0 fade in" style="border-radius: 0px">
                                    <a href="#" class="close" data-dismiss="alert"></a>
                                    <?= $this->session->flashdata('akun') ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php else: ?>

                            <?php if ($this->session->flashdata('akun')): ?>
                                <div class="alert alert-<?= $this->session->flashdata('alert') ?> mx-0 fade in" style="border-radius: 0px">
                                    <a href="#" class="close" data-dismiss="alert"></a>
                                    <?= $this->session->flashdata('akun') ?>
                                </div>
                            <?php endif; ?>
                            <form  role="form" id="form-reactivated" action="<?=base_url('akunv2/aktivasi') ?>" method="POST">
                                <div class="col-md-12"> 
                                    <div class="text-center mb-3">
                                        <label class="fw-bold">Aktivasi Ulang</label><br>
                                        <small for="aktivasi" class="fw-lighter">Silahkan masukkan Nomor KTP anda yang terdaftar.</small>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input required type="text" id="ktp_reactivated" name="ktp_reactivated" class="form-control fadeIn first text-center angka" maxlength="16" placeholder="Nomor KTP" autofocus>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-sm w-100 text-white" value="Kirim"><i class="far fa-paper-plane"></i> KIRIM</button>
                                    </div>
                                </div>
                            </form>

                        <?php endif ?>
                    </div>
                    <div class="modal-footer d-flex bd-highlight">
                        <a class="me-auto bd-highlight" onclick="login()" >Kembali</a>
                        <a class="bd-highlight" data-bs-dismiss="modal">Tutup</a>
                    </div>
                </div>
            </div>
        </div>
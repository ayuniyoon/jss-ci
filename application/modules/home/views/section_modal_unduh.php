<div class="modal fade" id="modalUnduh" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center mb-4">
                    <a target="_blank" href="https://play.google.com/store/apps/details?id=id.go.jogjakota.jogjasmartservice">
                        <img class="img-responsive" width="175" height="175" src="https://jss.jogjakota.go.id/manajemen/asset/img/jss.jpg" alt="qr-code">
                        <h5>Unduh Aplikasi JSS</h5>
                    </a>
                    <p>Silahkan Scan QRCode atau klik link diatas <br/>untuk mengunduh aplikasi.</p>
                </div>  
            </div>
            <div class="modal-footer d-flex bd-highlight">
                <a class="me-auto bd-highlight" onclick="login()" >Kembali</a>
                <a class="bd-highlight" data-bs-dismiss="modal">Tutup</a>
            </div>
        </div>
    </div>
</div>
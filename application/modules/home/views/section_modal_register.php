<div class="modal fade" id="modalDaftar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="mb-4">
                    <ul class="nav justify-content-center login-menu">
                        <li class="nav-item">
                            <a class="px-2" onclick="login()">MASUK</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2 active border-bottom" aria-current="page" onclick="register()">DAFTAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" onclick="unduh()">UNDUH APK</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" href="<?=base_url('panduan');?>">PANDUAN</a>
                        </li>
                        <li class="nav-item">
                            <a class="px-2" href="<?=base_url('faq');?>">FAQ</a>
                        </li>
                    </ul>
                </div>
                <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="message"></div>
                </div>
                <form id="form-register" role="form" action="#" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <div class="mb-3">
                                   <label class="fw-bold">Pilih Metode Aktivasi Akun.</label><br>
                                   <small for="aktivasi" class="fw-lighter">Bagi pengguna yang tidak memiliki Whatsapp, silahkan pilih metode aktivasi via Email.</small>
                               </div>
                                <div class="row">
                                    <div class="col-md-12 btn-group text-center">
                                        <a type="radio" class="btn btn-outline-primary btn-md col-md-6" name="verify" data-toggle="aktivasi" data-title="WA" value="WA" ><i class="fab fa-whatsapp fa-md"></i> &nbsp;Saya Pilih Whatsapp</a>

                                        <a type="radio" class="btn btn-outline-primary btn-md col-md-6" name="verify" data-toggle="aktivasi" data-title="EM" value="EM" ><i class="far fa-envelope fa-md"></i> &nbsp;Saya Pilih Email</a>
                                    </div>
                                    <input type="hidden" name="aktivasi" id="aktivasi" value="">
                                </div>
                            </div>
                            <p id="d2" style="color:red"></p> 
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-2">
                                <div class="input-group">
                                    <input type="text" class="form-control angka" name="nik" id="nik" placeholder="Masukkan 16 Digit NIK" required value="" title="Masukkan 16 Digit NIK" maxlength="16" autofocus>
                                    <span name="btn-cari" class="input-group-btn">
                                        <button class="input-group-text" id="btn-cari" type="button" onclick="gotocari()">Cari</button>
                                    </span>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required>
                            </div>
                            <div class="input-group mb-3">
                                <textarea rows="2" class="form-control" name="alamat" id="alamat" minlength="15" placeholder="Alamat Rumah" required data-toggle="tooltip" data-placement="top" title="Alamat diisi sesuai Alamat di KTP"></textarea>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control angka" name="no_telp" id="no_telp" placeholder="Nomor Handphone Aktif" required value="" maxlength="15" data-toggle="tooltip" data-placement="top" title="Nomor Handphone Aktif">
                            </div>
                            <div class="input-group mb-3">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Alamat Email" value="" data-toggle="tooltip" data-placement="top" title="Email ini digunakan untuk Promosi Layanan JSS (dapat dikosongkan)">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="nama_akun" id="nama_akun" minlength="4" placeholder="Username untuk Login" required value="" data-toggle="tooltip" data-placement="top" title="Username tidak mengandung spasi">
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" autocomplete="false" class="form-control" minlength="8" name="kata_sandi" id="kata_sandi" placeholder="Kata Sandi" required value="" data-toggle="tooltip" data-placement="top" title="Kata sandi minimal 8 karakter">
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" autocomplete="false" class="form-control" minlength="8" name="passconf" id="passconf" placeholder="Masukkan Ulang Kata Sandi" required value="" data-toggle="tooltip" data-placement="top" title="Harus diisi sama dengan kata sandi">
                            </div>
                            <div class="mx-auto">
                                <div id="g-recaptcha" class="g-recaptcha" data-sitekey="6LeiMNsZAAAAAEyaIV5Z2IWL5fh2bK6W58W9_cIO" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3 mb-3 text-center">
                            <input type="hidden" name="isvalid" value="">
                            <div class="checkbox fw-lighter">
                                <input type="checkbox" required> Dengan ini saya menyetujui semua kebijakan, <a href="<?=base_url('privacy_policy.html')?>">syarat dan ketentuan</a> yang berlaku
                            </div>
                        </div>
                        <div class="text-center mb-2">
                            <button type="button" class="btn btn-sm btn-primary text-white"> KIRIM PERMINTAAN AKUN !</button>
                            <button type="button" class="btn btn-sm btn-danger text-white"> BATAL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

 <!-- <div class="modal fade" id="daftarv2s" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
    <div class="modal-dialog modal-lg fadeInDown">
        <div class="modal-content">
            <div class="modal-body">
                <div class="">
                    <div id="formRegister-v2">
                        <div>
                            <a onclick="login()"><h2 class="inactive underlineHover pointer">Masuk </h2></a>
                            <h2 class="active pointer"> Daftar</h2>
                            <a onclick="unduh()"><h2 class="inactive underlineHover pointer">Unduh APK</h2></a>
                            <a href="<?=base_url();?>panduan"><h2 class="inactive underlineHover pointer">Panduan</h2></a>
                            <a href="<?=base_url();?>faq"><h2 class="modal-menu inactive underlineHover pointer">FAQ</h2></a>
                        </div>
                        <div class="alert-danger mt-1" style="display: none;">
                            <div class="message" style="padding: 10px 10px 10px 10px;"></div>
                        </div>
                        <?php if ($this->session->flashdata('register')): ?>
                            <div class="alert alert-danger fade in" style="border-radius: 0px">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <?= $this->session->flashdata('register') ?>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <form class="form-register mt-1" action="<?=base_url('akunv2/daftar/register');?>" method="POST" id="form-register" enctype="multipart/form-data">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Pilih Metode Aktivasi Akun.</label><br>
                                        <small for="aktivasi" class="control-label">Bagi pengguna yang tidak memiliki Whatsapp, silahkan pilih metode aktivasi via Email.</small>
                                        <div class="row">
                                            <div id="radioBtn" class="col-md-12 btn-group text-center">
                                                <a type="radio" class="btn btn-primary btn-sm col-md-6 <?= $_SESSION['aktivasi'] == "WA" ? "active" : "notActive"; ?>" name="verify" data-toggle="aktivasi" data-title="WA" value="WA" ><i class="fa fa-whatsapp fa-lg"></i> &nbsp;Saya Pilih Whatsapp</a>
                                                <a type="radio" class="btn btn-primary btn-sm col-md-6 <?= $_SESSION['aktivasi'] == "EM" ? "active" : "notActive"; ?>" name="verify" data-toggle="aktivasi" data-title="EM" value="EM" ><i class="fa fa-envelope-o fa-lg"></i> &nbsp;Saya Pilih Email</a>
                                            </div>
                                            <input type="hidden" name="aktivasi" id="aktivasi" value="">
                                        </div>
                                    </div>
                                    <p id="d2" style="color:red"></p> 
                                </div>
                                <div class="data">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control angka" name="nik" id="nik" placeholder="Masukkan 16 Digit NIK" required value="<?=$this->session->userdata('kependudukan')['nik']?>" data-toggle="tooltip" data-placement="top" title="Masukkan 16 Digit NIK" maxlength="16" autofocus>
                                                <span name="btn-cari" class="input-group-btn">
                                                    <button class="btn btn-default btn-group" id="btn-cari" type="button" onclick="gotocari()">Cari</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required value="<?=$this->session->userdata('kependudukan')['nama']?>" data-toggle="tooltip" data-placement="top" title="Nama Lengkap diisi sesuai nama di KTP (Tanpa Gelar & Tidak Disingkat)">
                                        </div>
                                        <div class="form-group">
                                            <textarea rows="2" class="form-control" name="alamat" id="alamat" minlength="15" placeholder="Alamat Rumah" required data-toggle="tooltip" data-placement="top" title="Alamat diisi sesuai Alamat di KTP"><?=$this->session->userdata('kependudukan')['alamat']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control angka" name="no_telp" id="no_telp" placeholder="Nomor Handphone Aktif" required value="<?=$this->session->userdata('kependudukan')['no_telp']?>" maxlength="15" data-toggle="tooltip" data-placement="top" title="Nomor Handphone Aktif">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Alamat Email" value="<?=$this->session->userdata('kependudukan')['email']?>" data-toggle="tooltip" data-placement="top" title="Email ini digunakan untuk Promosi Layanan JSS (dapat dikosongkan)">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="nama_akun" id="nama_akun" minlength="4" placeholder="Username untuk Login" required value="<?=$this->session->userdata('kependudukan')['username']?>" data-toggle="tooltip" data-placement="top" title="Username tidak mengandung spasi">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" autocomplete="false" class="form-control" minlength="8" name="kata_sandi" id="kata_sandi" placeholder="Kata Sandi" required value="" data-toggle="tooltip" data-placement="top" title="Kata sandi minimal 8 karakter">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" autocomplete="false" class="form-control" minlength="8" name="passconf" id="passconf" placeholder="Masukkan Ulang Kata Sandi" required value="" data-toggle="tooltip" data-placement="top" title="Harus diisi sama dengan kata sandi">
                                        </div>
                                        <div class="mx-auto">
                                            <div id="g-recaptcha" class="g-recaptcha" data-sitekey="6LeiMNsZAAAAAEyaIV5Z2IWL5fh2bK6W58W9_cIO" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="hidden" name="isvalid" value="<?=$_SESSION["not_valid"]?>">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" required> Dengan ini saya menyetujui semua kebijakan, <a href="<?=base_url('privacy_policy.html')?>">syarat dan ketentuan</a> yang berlaku
                                            </label>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-md" id="btn-register">Kirim Permintaan Akun</button>
                                        <a type="button" class="btn btn-danger btn-md" id="btn-register-close" data-dismiss="modal">Tutup</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
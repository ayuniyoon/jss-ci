<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="#" class="logo me-auto text-white">
            <img src="<?= base_url()?>assets/img/logo.png" alt="" class="img-fluid">
        </a>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <?php if (!empty($jsonuser)): ?>
                    <li class="active"><a href="#">Beranda</a></li>
                    <li><a href="#">Scan QR</a></li>
                    <li><a href="#">Dokumentasi</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Tentang</a></li>
                    <li class="drop-down">
                        <a href="#" class="nav-profile" data-aos-delay="300">
                            <span>
                                JSS-B2739
                                <img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg" width="25" height="25" class="rounded-circle">
                            </span>
                        </a>
                        <ul class="profile" role="menu">
                            <li class="name">Drs. SRI WAHYUNI LOREM DOLOR S.Kom, M.kom</li>
                            <li class="jabatan">Petugas Lapangan / Pegawai</li>
                            <li class="dinas">DINAS KOMUNIKASI INFORMATIKA DAN PERSANDIAN</li>
                            <li class="dropdown-divider my-3"></li>
                            <li class="jss-qrcode my-2" data-bs-toggle="modal" data-bs-target="#modal-qrcode"><a href="#"><i class="fas fa-qrcode"></i> JSS Code</a></li>
                            <li class="my-2"><a href="#"><i class="fas fa-users-cog"></i> Pengaturan Akun</a></li>
                            <li class="text-center mt-3">
                                <div class="sign-out">Keluar</div>
                            </li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li></li>
                    <li><a class="" onclick="register()">DAFTAR</a></li>
                    <li><a class="" onclick="login()">MASUK</a></li>
                <?php endif ?>
            </ul>
        </nav>
        <!-- .nav-menu -->
    </div>
</header>
<!-- End Header -->

<div class="modal fade" id="modal-qrcode" tabindex="-1" aria-labelledby="qrcodeLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="profile-qrcode d-flex justify-content-center" id="profile-qrcode"></div>
            </div>
        </div>
    </div>  
</div>
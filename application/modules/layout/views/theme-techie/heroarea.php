<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container-fluid" data-aos="fade-up">

        <div class="owl-carousel hero-carousel mx-0 mx-lg-2">
            <div class="row justify-content-center" style="bottom: -10px;">
                <div class="col-xl-7 col-lg-7 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Vaksinasi Sudah Dimulai</h1>
                    <h2>Kota Yogyakarta sudah menyiapkan Program vaksinasi Covid-19 dan akan mulai dilaksanakan pada Jumat 15 Januari 2021</h2>
                    <div class=""><a href="#" class="btn-get-started scrollto">Selengkapnya</a></div>
                </div>
                <div class="col-xl-4 col-lg-5 order-1 order-lg-2 hero-img d-flex justify-content-center" data-aos="zoom-in" data-aos-delay="150">
                    <img src="<?= base_url()?>assets/img/apd.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-7 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Vaksin tidak membuatmu jadi mutan</h1>
                    <h2>Kota Yogyakarta sudah menyiapkan Program vaksinasi Covid-19 dan akan mulai dilaksanakan pada Jumat 15 Januari 2021</h2>
                    <div class=""><a href="#" class="btn-get-started scrollto">Selengkapnya</a></div>
                </div>
                <div class="col-xl-4 col-lg-5 order-1 order-lg-2 hero-img d-flex justify-content-center" data-aos="zoom-in" data-aos-delay="150">
                    <img src="<?= base_url()?>assets/img/walkot.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<main id="main">
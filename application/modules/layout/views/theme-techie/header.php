<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>JSS ~ Pemerintah Kota Yogyakarta</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="<?= base_url()?>assets/img/favicon.png" rel="icon">
    <link href="<?= base_url()?>assets/img/logo.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="<?= base_url()?>assets/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/node_modules/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/node_modules/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/vendor/fontawesome/css/all.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/vendor/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/vendor/holdon/HoldOn.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/login.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/animate.min.css" rel="stylesheet">
    <script src="<?= base_url()?>assets/vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        var base_url = '<?=base_url()?>';
    </script>
    <script src="<?= base_url()?>assets/js/app.js"></script> 
</head>

<body>  
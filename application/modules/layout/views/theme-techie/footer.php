</main>
<!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer" class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-6 footer-contact">
                    <a href="#" class="footer-logo text-white d-flex">
                        <img src="<?= base_url()?>assets/img/logo.png" alt="" class="img-fluid img-content-fit h-100">
                        <h3>Jogja Smart Service</h3>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Apa sih JSS itu ?</h4>
                    <p>Jogja Smart Service adalah Balaikota Virtual atau Portal maya Pemerintah Kota Yogyakarta dalam rangka memberikan layanan langsung kepada semua masyarakat di Kota Yogyakarta.</p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Satu Pintu  </h4>
                    <p>Daftar layanan Pemerintah Kota yang dapat diakses langsung oleh masyarakat dengan mengedepankan pelayanan mandiri (Swalayan).</p>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <div class="text-center">
                        <a href="#" target="_blank" class="text-white">
                            <img src="<?= base_url()?>assets/img/ios.png" alt="" class="img-fluid img-content-fit h-100">
                        </a>
                    </div>
                    <div class="text-center">
                        <a href="#" target="_blank" class="text-white">
                            <img src="<?= base_url()?>assets/img/gplay.png" alt="" class="img-fluid img-content-fit h-100">
                        </a>
                    </div>
                    <div class="social-links text-center text-md-right pt-3 pt-md-2">
                        <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">

        <div class="copyright-wrap d-md-flex py-4">
            <div class="me-md-auto text-center text-md-start">
                <div class="copyright">
                    <strong><span>Jogja Smart Service.</span></strong>
                </div>
                <div class="credits">
                    &copy; 2021 <a href="https://jogjakota.go.id/">Jogjakota.go.id</a>. All right reserved. Hak Cipta <a href="#">Diskominfo</a>
                </div>
            </div>
        </div>

    </div>
</footer>
<!-- End Footer -->

  <!-- <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a> -->
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="<?= base_url()?>assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url()?>assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?= base_url()?>assets/vendor/aos/aos.js"></script>
  <script src="<?= base_url()?>assets/vendor/holdon/HoldOn.min.js"></script>
  <script src="<?= base_url()?>assets/js/main.js"></script> 

  <script type="text/javascript">
      $(document).ready(function() {

        $(".menu").hide();
        $(".menus-edit").on("click", function() {
            $(".menu").show();
            $(".menus-edit").hide();
            $(".menus-done").show();
        });
        $(".menus-done").on("click", function() {
            $(".menu").hide();
            $(".menus-edit").show();
            $(".menus-done").hide();
        });
        $(".fav-delete").on("click", function() {
            Swal.fire({
                title: 'Apakah Anda yakin ?',
                text: "Proses ini akan menghapus data yang anda pilih !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus !',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Terhapus !',
                        'Datamu sudah dihapus.',
                        'success'
                        )
                }
            })       
        });
        $(".jss-qrcode").on("click", function() {
            $("#modal-qrcode").show();

         // $.ajax({
    //     url: '<?=base_url("homepage/app")?>',
    //     type: 'post',
    //     data: $('#search').serialize(),
    //     dataType: 'json',
    //     success: function(data){
    //     }
    // });

    $("#profile-qrcode").html("");
    $("#profile-qrcode").html('<img src="<?=base_url()?>/assets/img/qrcode.png" alt="" class="img-fluid">');

});

        $(".sign-out").on("click", function() {
            Swal.fire({
                title: 'Keluar Aplikasi ?',
                text: "Yakin Anda ingin keluar dari Aplikasi JSS ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Segera !',
                cancelButtonText: 'Batal',
            }).then((result) => {
            // if (result.isConfirmed) {
            //     Swal.fire(
            //         'Baiklah !',
            //         'Mohon tunggu hingga .',
            //         'success'
            //         )
            // }
        })       
        });

        $('input[name="keyword"]').keyup(delay(function (e)
        {
            HoldOn.open(options);

            if ($('input[name="keyword"]').val() == "") {
                $('#panel-category').show();
                $('#panel-search-app').hide();
            } else {
                $('#panel-category').hide();
                $('#panel-search-app').show();
            // getapp();
        }
        HoldOn.close();

    }, 500));



    });

      function getapp()
      {
    // $.ajax({
    //     url: '<?=base_url("homepage/app")?>',
    //     type: 'post',
    //     data: $('#search').serialize(),
    //     dataType: 'json',
    //     success: function(data){
    //     }
    // });
    $("#panel-search-app").html("");

    var html = '';
    $("#panel-search-app").html(html);
}
</script>

</body>

</html>
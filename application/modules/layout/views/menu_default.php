<!--nav-->
<nav class="navbar navbar-default navbar-custom bg-white" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand " href="https://jss.jogjakota.go.id/beranda/main" style="height: 60px;width: auto;">
                <img src="<?= base_url(); ?>asset/img/logo_pemkot.png" class="img-responsive" style="height: 60px;width: auto;">
            </a>
        </div>
        <?php if (isset($public) == false) { ?>
            <div class="collapse navbar-collapse" style="height: 1px;">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="nav-link" href="<?= base_url('beranda/main'); ?>">
                            Beranda
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?= base_url('qrcode/daring'); ?>">
                            Scanner QRcode
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?= base_url('beranda/dokumentasi'); ?>">
                            Dokumentasi
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?= base_url('faq'); ?>">
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a class="nav-link pointer" href="#">
                            Tentang
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?= base_url('beranda/akun'); ?>">
                            Akun
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?= base_url('auth/signoutJSS'); ?>">
                            Keluar
                        </a>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </div>
</nav>
<!-- top content -->

<div class="docs-header">
    <div class="topic" style="padding: 30px 0 30px;">
        <div class="container">
            <div class="col-md-10">
                <h3 class="fadeIn first"><?= $title; ?></h3>
                <h4 class="fadeIn second"><?= $subtitle; ?></h4>
            </div>
            <div class="col-md-2">
                <h4 class="fadeIn second"></h4>
            </div>
        </div>
    </div>
</div>
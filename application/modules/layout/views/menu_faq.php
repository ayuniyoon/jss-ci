    <nav class="navbar navbar-default navbar-custom bg-white" role="navigation">
         <div class="container" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand " href="<?=base_url('cms_faq')?>" style="height: 60px;width: auto;">
                    <img src="<?=base_url();?>asset/img/logo_pemkot.png" class="img-responsive" style="height: 60px;width: auto;">
                </a>
            </div>
            <?php if (isset($public) == false) { ?>
            <div class="collapse navbar-collapse" style="height: 1px;">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="nav-link" href="<?=base_url('beranda/main');?>">
                            Beranda JSS
                        </a>
                    </li>
                    <li>
                        <a class="nav-link <?=$home?>" href="<?=base_url('cms_faq');?>">
                            Beranda FAQ
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle <?=$master?>" data-toggle="dropdown" href="#">
                            Master Data <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu navbar-left">
                            <li><a href="<?=base_url('cms_faq/category');?>">Kategori</a></li>
                            <li><a href="<?=base_url('cms_faq/sub_category');?>">Sub Kategori</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle <?=$faq?>" data-toggle="dropdown" href="#">
                            Manajemen FAQ <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu navbar-left">
                            <li><a href="<?=base_url('cms_faq/question');?>">Pertanyaan</a></li>
                            <li><a href="<?=base_url('cms_faq/reply');?>">Jawaban</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle <?=$setting?>" data-toggle="dropdown" href="#">
                            Pengaturan <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu navbar-left">
                            <li><a href="<?=base_url('cms_faq/user');?>">Pengguna</a></li>
                            <li><a href="<?=base_url('cms_faq/guide');?>">Panduan</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-link" href="<?=base_url('auth/signoutJSS');?>">
                            Keluar
                        </a>
                    </li>
                </ul>
            </div>
            <?php } ?>
        </div> 
    </nav>
    
    <div class="docs-header">
        <div class="topic" style="padding: 30px 0 30px;">
            <div class="container">
                <h3 class="fadeIn first"><?=$title;?></h3>
                <h4 class="fadeIn second"><?=$subtitle;?></h4>
            </div>
            <br><br>
            <div class="topic__infos">
                <div class="container fadeIn third">
                    <?=$this->session->userdata('name')?> (<b><?=$this->session->userdata('jss_id')?></b>) - <?=$this->session->userdata('role_name')?> - <?=$this->session->userdata('skpd_name')?>
                </div>
            </div>
        </div>
    </div>
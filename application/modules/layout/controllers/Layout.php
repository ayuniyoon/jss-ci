<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends MX_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function header($header = NULL)
	{
		$this->load->view('theme-techie/header', $header);
	}
	function navbar($navbar = NULL)
	{
		$this->load->view('theme-techie/navbar', $navbar);
	}
	function heroarea($heroarea = NULL)
	{
		$this->load->view('theme-techie/heroarea', $heroarea);
	}
	function footer()
	{
		$this->load->view('theme-techie/footer');
	}

}
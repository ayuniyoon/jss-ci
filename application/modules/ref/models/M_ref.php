<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_ref extends CI_Model {

    /* -- ref broadcast -- */
    function get_jss_name($id)
    {
    	$otherdb = $this->load->database('dbupik', TRUE);

		$otherdb->select('nama');
		$otherdb->from('tbl_user');
		$otherdb->where('id_upik', $id);
		$data = $otherdb->get()->row_array();

		return $data['nama'];
    }

    function get_skpd_name($id)
    {
    	$otherdb = $this->load->database('dbupik', TRUE);

		$otherdb->select('nama_skpd');
		$otherdb->from('tbl_skpd');
		$otherdb->where('id_skpd', $id);
		$data = $otherdb->get()->row_array();

		return $data['nama_skpd'];
    }

    function get_districts_name($id)
    {
    	$otherdb = $this->load->database('dbupik', TRUE);

		$otherdb->select('kecamatan');
		$otherdb->from('kecamatan');
		$otherdb->where('id', $id);
		$data = $otherdb->get()->row_array();

		return $data['kecamatan'];
    }

    function get_villages_name($id)
    {
    	$otherdb = $this->load->database('dbupik', TRUE);

		$otherdb->select('kelurahan');
		$otherdb->from('kelurahan');
		$otherdb->where('id', $id);
		$data = $otherdb->get()->row_array();

		return $data['kelurahan'];
    }


    /* -- ref faq -- */
  	function check_user($id)
    {
        $otherdb = $this->load->database('dbupik', TRUE);
        
        $otherdb->where('user_status', 'Y');
        $otherdb->where('user_jss_id', $id);
        $otherdb->from('faq_user');

        return $otherdb->count_all_results();
    }

    function get_category_name($id)
    {
        $otherdb = $this->load->database('dbupik', TRUE);

        $otherdb->select('category_name AS `name`');
        $otherdb->from('faq_category');
        $otherdb->where('category_id', $id);
        $data = $otherdb->get()->row_array();

        return $data['name'];
    }

    function upikmySQL($sql)
    {
        $dbupik = $this->load->database('dbupik', TRUE);

        return $dbupik->query($sql);
    }

    function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }
}